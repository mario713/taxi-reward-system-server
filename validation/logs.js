const Joi = require('@hapi/joi');

const LogsValidation = {}

LogsValidation.User = Joi.object({
    userid: Joi.number
});

LogsValidation.getLogs = Joi.object({
    userid: Joi.number()
        .required()
        .min(1),
    page: Joi.number()
        .required()
        .min(0),
    limit: Joi.number()
        .required()
        .min(5)
        .max(50)
});

LogsValidation.countPages = Joi.object({
    id:     Joi.number()
                .required()
                .min(1),
    limit: Joi.number()
            .required()
            .min(5)
            .max(50)
});

module.exports = LogsValidation;