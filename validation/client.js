const Joi = require('@hapi/joi');

const ClientValidation = {}

ClientValidation.ID = Joi.object({
        id: Joi.number()
                .required()
                .min(0)
});

ClientValidation.getPage = Joi.object({
        page: Joi.number()
            .required()
            .min(0),
        limit: Joi.number()
            .required()
            .min(5)
            .max(50)
});

ClientValidation.countPages = Joi.object({
    limit: Joi.number()
            .required()
            .min(5)
            .max(50)
});

ClientValidation.getGratis = Joi.object({
    page: Joi.number()
            .required()
            .min(0),
    limit: Joi.number()
            .required()
            .min(5)
            .max(50)
});

ClientValidation.updateClient = Joi.object({
        gratis: Joi.number()
                .required()
                .min(0),
        name: Joi.string()
                .max(30)
                .pattern(new RegExp('^[a-zA-Z]*$'))
                .allow('', null),
        lastName: Joi.string()
                .min(2)
                .max(30)
                .pattern(new RegExp('^[a-zA-Z]*$'))
                .allow('', null),
        phoneNumber: Joi.string()
                .min(1)
                .max(255)
                .pattern(new RegExp('^[0-9+,\.]*$'))
                .allow('', null)
});

module.exports = ClientValidation;