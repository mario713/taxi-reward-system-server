const Joi = require('@hapi/joi');

const SettingsValidation = {}

SettingsValidation.updateSettings = Joi.object({
    title: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9 ]*$')),
    language: Joi.string()
                .pattern(new RegExp('^[a-z]*$'))
                .required()
});

module.exports = SettingsValidation;