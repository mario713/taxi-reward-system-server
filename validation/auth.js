const Joi = require('@hapi/joi');

const authValidation = {}

authValidation.register = Joi.object({
    username: Joi.string()
            .required()
            .min(3)
            .max(16)
            .pattern(new RegExp('^[a-zA-Z0-9]{3,16}$')),
    email: Joi.string()
            .required()
            .email(),
    password: Joi.string()
            .required()
            .min(6)
            .max(72)
});

authValidation.login = Joi.object({
    username: Joi.string()
            .required()
            .min(3)
            .max(16),
    password: Joi.string()
            .required()
            .min(1)
            .max(72)
});

module.exports = authValidation;