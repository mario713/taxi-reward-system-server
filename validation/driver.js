const Joi = require('@hapi/joi');

const driverValidation = {}

driverValidation.addDriver = Joi.object({
    name: Joi.string()
            .required()
            .max(30)
            .pattern(new RegExp('^[a-zA-Z]*$')),
    lastName: Joi.string()
            .min(2)
            .max(30)
            .pattern(new RegExp('^[a-zA-Z]*$')),
    number: Joi.number()
            .required()
            .min(1),
    phoneNumber: Joi.string()
            .min(1)
            .max(255)
            .pattern(new RegExp('^[0-9+,\.]*$'))
            .allow('', null)
});

driverValidation.updateDriver = driverValidation.addDriver;

    
driverValidation.getDriver = Joi.object({
    id: Joi.number()
            .required()
            .min(1)
});

driverValidation.deleteDriver = Joi.object({
    id: Joi.number()
            .required()
            .min(1)
});

driverValidation.ID = driverValidation.deleteDriver;

module.exports = driverValidation;