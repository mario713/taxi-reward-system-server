const Joi = require('@hapi/joi');

const CourseValidation = {}

CourseValidation.ID = Joi.object({
    id: Joi.number()
        .required()
        .min(1)
});

CourseValidation.addCourse = Joi.object({
    client: Joi.number()
            .required()
            .min(1),
    driver: Joi.number()
            .required()
            .min(1)
});

CourseValidation.editCourse = CourseValidation.addCourse;

CourseValidation.getCourses = Joi.object({
    page: Joi.number()
            .required()
            .min(0),
    limit: Joi.number()
            .required()
            .min(5)
            .max(50)
});

CourseValidation.countPages = Joi.object({
        limit: Joi.number()
                .required()
                .min(5)
                .max(50)
});
module.exports = CourseValidation;