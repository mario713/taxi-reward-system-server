const Joi = require('@hapi/joi');

const userValidation = {}

userValidation.ID = Joi.object({
    id: Joi.number()
        .min(1) 
});

userValidation.addUser = Joi.object({
    username:   Joi.string()
                    .required()
                    .min(3)
                    .max(15)
                    .pattern(new RegExp('^[a-zA-Z0-9]{3,16}$')),
    email:  Joi.string()
                .required()
                .min(5)
                .max(255)
                .pattern(new RegExp('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,9}')),
    password:  Joi.string()
                .required()
                .min(8)
                .max(72),
    rank:   Joi.number()
                .required()
                .min(1)
});

userValidation.updateUser = Joi.object({
    username:   Joi.string()
                    .required()
                    .min(3)
                    .max(16)
                    .pattern(new RegExp('^[a-zA-Z0-9]{3,16}$')),
    email:  Joi.string()
                .required()
                .min(5)
                .max(255)
                .pattern(new RegExp('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,9}')),
    password:  Joi.string()
                .min(8)
                .max(72),
    rank:   Joi.number()
                .required()
                .min(1)
});

module.exports = userValidation;