const Joi = require('@hapi/joi');

const rankValidation = {}

rankValidation.getRank = Joi.object({
    id: Joi.number()
            .required()
            .min(1)
});

rankValidation.addRank = Joi.object({
    name: Joi.string()
            .required()
            .min(3)
            .max(32)
            .pattern(new RegExp('^[a-zA-Z0-9]{3,32}$')),
    color: Joi.string()
            .required()
            .min(7)
            .max(7)
            .pattern(new RegExp('^#[a-fA-F0-9]{6}$')),
    perms: Joi.string()
            .required()
            .pattern(new RegExp('^(user|admin)$')),
});

rankValidation.updateRank = rankValidation.addRank;

rankValidation.ID = Joi.object({
        id: Joi.number()
                .required()
                .min(1)
})

rankValidation.deleteRank = Joi.object({
    id: Joi.number()
            .required()
            .min(1)
});

module.exports = rankValidation;