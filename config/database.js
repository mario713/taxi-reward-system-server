require('dotenv/config');
const Sequelize = require('sequelize');

module.exports = new Sequelize({
    dialect: 'mariadb',
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    dialectOptions: {
        timezone: process.env.TIMEZONE
    },
    define: {
        timestamps: false
    },
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

