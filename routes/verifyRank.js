const jwt = require('jsonwebtoken');
const Rank = require('../models/Rank');
const User = require('../models/User');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = async (req, res, next) => {
    const token = req.header('auth-token');
    const decodedToken = jwt.decode(token);

    const user = await User.findOne({
        attributes: ['rank'],
        where: {
            id: decodedToken.id
        }
    });

    const rank = await Rank.findOne({
        where: {
            id: user.rank
        }
    });

    if (rank.perms != 'admin') return res.status(403).json({message: 'Insufficient permissions!'});
    next();
}