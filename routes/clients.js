const express = require('express');
const router = express.Router();
const clientController = require('../controllers/client');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/getGratis/:page/:limit', authCheck, clientController.getGratis);
router.get('/getGratisPages/:limit', authCheck, clientController.countGratisPages);
router.get('/get/:page/:limit', authCheck, clientController.getSome);
router.get('/getOne/:number', authCheck, clientController.getOne);
router.get('/getClientPages/:limit', authCheck, clientController.countClientsPages);
router.get('/use/:number', authCheck, clientController.useGratisCourse);
router.post('/:number', authCheck, clientController.updateClient);
router.get('/search/:number', authCheck, clientController.findClients);


module.exports = router;