const express = require('express');
const router = express.Router();
const SettingsController = require('../controllers/settings');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', SettingsController.get);
router.post('/', [authCheck, rankCheck], SettingsController.update);

module.exports = router;