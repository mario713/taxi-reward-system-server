const express = require('express');
const router = express.Router();
const UsersController = require('../controllers/users');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', [authCheck, rankCheck], UsersController.getAll);
router.post('/', [authCheck, rankCheck], UsersController.addUser);
router.get('/:id', authCheck, UsersController.getUser);
router.delete('/:id', [authCheck, rankCheck], UsersController.deleteUser);
router.put('/:id', [authCheck, rankCheck], UsersController.updateUser);

module.exports = router;