const express = require('express');
const router = express.Router();
const driversController = require('../controllers/drivers');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', authCheck, driversController.getAll);
router.post('/', [authCheck, rankCheck], driversController.addDriver);
router.get('/:id', authCheck, driversController.getDriver);
router.delete('/:id', [authCheck, rankCheck], driversController.deleteDriver);
router.put('/:id', [authCheck, rankCheck], driversController.updateDriver);

module.exports = router;