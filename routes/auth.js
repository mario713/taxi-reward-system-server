const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/auth');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', async (req, res) => {
    res.json({message: 'Auth home page.'});
});

router.post('/register', [authCheck, rankCheck], AuthController.register);
router.post('/login', AuthController.login);
router.get('/logout', AuthController.logout);

module.exports = router;