const jwt = require('jsonwebtoken');
const UserSession = require('../classes/UserSession');

module.exports = async (req, res, next) => {
    const token = req.header('auth-token');
    if(!token) return res.status(401).json({message: 'Access denied! No token found!'});

    try { 
        let dToken = jwt.decode(token, process.env.JWT_SECRET);
        const verified = jwt.verify(token, process.env.JWT_SECRET);

        if(!verified) { throw 'Invalid Token!'; }

        let validation;
        await UserSession.validate(dToken.sessionid, dToken.id).then((result) => {
            validation = result;
        });
        if(!validation){ throw 'Invalid Session!'; }

        req.user = verified;
        next();
    } catch(error) {
        res.status(401).json({message: error});
    }
}