const express = require('express');
const router = express.Router();
const RanksController = require('../controllers/ranks');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', authCheck, RanksController.getAll);
router.get('/:id', authCheck, RanksController.getRank);
router.post('/', [authCheck, rankCheck], RanksController.addRank);
router.delete('/:id', [authCheck, rankCheck], RanksController.deleteRank);
router.put('/:id', [authCheck, rankCheck], RanksController.updateRank);

module.exports = router;