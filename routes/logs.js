const express = require('express');
const router = express.Router();
const LogsController = require('../controllers/logs');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/userPages/:id/:limit', [authCheck, rankCheck], LogsController.getUserLogPages);
router.get('/user/:id/:page/:limit', [authCheck, rankCheck], LogsController.getUserLogs);
router.get('/driverPages/:id/:limit', [authCheck, rankCheck], LogsController.getDriverLogPages);
router.get('/driver/:id/:page/:limit', [authCheck, rankCheck], LogsController.getDriverLogs);
router.get('/clientPages/:number/:limit', [authCheck, rankCheck], LogsController.getClientLogPages);
router.get('/client/:number/:page/:limit', [authCheck, rankCheck], LogsController.getClientLogs);

module.exports = router;
