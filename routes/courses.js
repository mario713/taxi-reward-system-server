const express = require('express');
const router = express.Router();
const coursesController = require('../controllers/courses');
const authCheck = require('./verifyToken');
const rankCheck = require('./verifyRank');

router.get('/', authCheck, coursesController.getLast);
router.post('/add', authCheck, coursesController.addCourse);
router.get('/get/:page/:limit', authCheck, coursesController.getCourses);
router.get('/getOne/:id', authCheck, coursesController.getCourse);
router.post('/edit/:id', authCheck, coursesController.updateCourse);
router.delete('/delete/:id', authCheck, coursesController.deleteCourse);
router.get('/pages/:limit', authCheck, coursesController.countPages);

module.exports = router;