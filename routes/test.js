const express = require('express');
const router = express.Router();
const testControlller = require('../controllers/test');
const authCheck = require('./verifyToken');
//const rankCheck = require('./verifyRank');

router.get('/', authCheck, testControlller.test);

module.exports = router;