const express = require('express');
const app = express();
const cors = require('cors');
require('dotenv/config');

//Database
const db = require('./config/database');


//Middleware
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json());

//Routes
app.get('/', (req, res) => res.send('Hello World!'));
app.use('/auth', require('./routes/auth'));
app.use('/users', require('./routes/users'));
app.use('/drivers', require('./routes/drivers'));
app.use('/ranks', require('./routes/ranks'));
app.use('/courses', require('./routes/courses'));
app.use('/clients', require('./routes/clients'));
app.use('/settings', require('./routes/settings'));
//app.use('/logs', require('./routes/logs'));

db.sync();

//Error handling
app.use((req, res, next) => {
    const error = new Error('[404] Page Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

//Starting the server
app.listen(process.env.PORT || 3000);

console.log('Server started on port ' + (process.env.PORT || 3000));