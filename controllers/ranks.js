const Rank = require('../models/Rank');
const User = require('../models/User');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const rankValidation = require('../validation/rank');
const jwt = require('jsonwebtoken');
//const UserLog = require('../classes/logs/User');

const RanksController = {}

RanksController.getAll = async (req, res) => {
    //Get all ranks
    const ranks = await Rank.findAll({
        attributes: ['id', 'name', 'color', 'perms', 'isDefault']
    });
    return res.status(200).json(ranks);
}

RanksController.getRank = async (req, res) => {
    //Validate
    const { error } = rankValidation.getRank.validate({id: req.params.id});
    if (error) return res.status(400).json({error});

    //Get rank with given ID
    const rank = await Rank.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!rank) return res.status(400).json({message: 'error.rank.notFound'})

    return res.status(200).json(rank);
}

RanksController.addRank = async (req, res) => {

    //Validate
    const { error } = rankValidation.addRank.validate(req.body);
    if (error) return res.status(400).json({error});

    //Remove #
    color = req.body.color.substr(1);

    //Create rank
    const rank = await Rank.create({
        name: req.body.name,
        color: color,
        perms: req.body.perms,
        isDefault: 0
    });

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'rank_added', 'Dodano nowa range ['+rank.id+'] '+req.body.name);

    res.status(200).json({message: 'permissions.creationSuccess'});
}

RanksController.deleteRank = async (req, res) => {
    //Validate
    const { error } = rankValidation.deleteRank.validate({id: req.params.id});
    if (error) return res.status(400).json({error});

    //Get rank with given ID
    const rankData = await Rank.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!rankData) return res.status(400).json({message: 'error.rank.notFound'})

    console.log(rankData.dataValues);
    if(rankData.dataValues.isPrime){
        return res.status(400).json({message: 'permissions.isPrime'});
    }
    if(rankData.dataValues.isDefault) {
        return res.status(400).json({message: 'permissions.isDefault'});
    }

    const assignedUsers = await User.count({
        where: {
            rank: req.params.id
        }
    });

    if(assignedUsers > 0) {
        return res.status(400).json({message: 'permissions.assignedUsers'});
    }

    //Delete rank
    const rank = await Rank.destroy({
        where: {
            id: req.params.id
        }
    });
    if (!rank) return res.status(400).json({message: `Rank with ID ${req.params.id} not found.`});

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'rank_deleted', 'Usunieto range ['+rankData.id+'] '+rankData.name);

    res.status(200).json({message: `Rank with ID ${req.params.id} has been deleted.`});
}

RanksController.updateRank = async (req, res) => {
    //Validate
    const params = rankValidation.ID.validate({id: req.params.id});
    if (params.error) return res.status(400).json({message: params.error});

    const { error } = rankValidation.updateRank.validate(req.body);
    if (error) return res.status(400).json({message: error});

    //Remove #
    color = req.body.color.substr(1);

    //Check if rank exists
    const rankExists = await Rank.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!rankExists) return res.status(400).json({code: 10, message: 'There is no rank in database with given ID!'});

    //Update rank
    const rank = await Rank.update({
        name: req.body.name,
        color: color,
        perms: req.body.perms
    },  {
        where: {
            id: req.params.id
        }
    });

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'rank_updated', 'Zaktualizowano range ['+rankExists.id+'] '+rankExists.name);

    return res.status(200).json({message: 'permissions.edit.updated', rankID: req.params.id});
}

RanksController.myself = async (req, res) => {
    const token = req.header('auth-token');
    const decodedToken = jwt.decode(token);

    me = await User.findOne({
        attributes: ['rank'],
        where: {
            id: decodedToken.id
        }
    });

    myrank = await Rank.findOne({
        where: {
            id: me.rank
        }
    });
    if (!myrank) return res.status(400).json({message: '[Critical Error] Rank not found.'});

    res.status(200).json({myrank});
}

module.exports = RanksController;