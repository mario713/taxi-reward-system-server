const Setting = require('../models/Setting');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const SettingsValidation = require('../validation/settings');

const newSetting = {
    title: process.env.DEFAULT_TITLE,
    language: process.env.DEFAULT_LANGUAGE
};

const SettingsController = {}

SettingsController.get = async (req, res) => {
 //return res.status(400).json({message: 'TEST'});
    let setting = await Setting.findOne({
        limit: 1,
        order: [
            ['id', 'ASC']
        ]
    });

    if(!setting) {
        setting = await Setting.create(newSetting);
        if(!setting) return res.status(500).json({message: 'Cant add settings!'});

        return res.status(200).json(newSetting);
    }

    return res.status(200).json(setting);
}

SettingsController.update = async (req, res) => {
    console.log(req.body);
    //Validate
    const { error } = SettingsValidation.updateSettings.validate(req.body);
    if (error) return res.status(400).json({error});

    let setting = await Setting.findOne({
        limit: 1,
        order: [
            ['id', 'ASC']
        ]
    });

    if(!setting) { 
        setting = await Setting.create(newSetting);
        if(!setting) return res.status(500).json({message: 'Cant add settings!'});
    }

    query = {}
    if (req.body.title) query.title = req.body.title;
    if (req.body.language) query.language = req.body.language;

    console.log(`Query: ${JSON.stringify(query)}`);

    const updatedSettings = await Setting.update(
        query, {
        where: {},
        limit: 1,
        order: [['id', 'ASC']]
    });
    if(!updatedSettings) return res.status(400).json({message: 'Setting couldnt be updated.'});

    console.log(`Settings: ${JSON.stringify(updatedSettings)}`);
    return res.status(200).json({message: 'settings.updateSuccess'});

}

module.exports = SettingsController;