const User = require('../models/User');
const Sequelize = require('sequelize');
const userValidation = require('../validation/user');
const bcrypt = require('bcrypt');
const Op = Sequelize.Op;
const Rank = require('../models/Rank');
const jwt = require('jsonwebtoken');
//const UserLog = require('../classes/logs/User');

const UsersController = {}

UsersController.getAll = async (req, res) => {
    //Get all users
    const users = await User.findAll({
        attributes: ['id', 'username', 'email', 'rank']
    });
    return res.status(200).json(users);
}

UsersController.getUser = async (req, res) => {
    //Validate
    const { error } = userValidation.ID.validate({id: req.params.id});
    if (error) return res.status(400).json({error});

    //Get user with given ID
    const user = await User.findOne({
        where: {
            id: req.params.id
        }
    });

    if(!user) return res.status(400).json({message: 'User not found!'});
    res.status(200).json(user);
}

UsersController.addUser = async (req, res) => {

    //Validate
    const { error } = userValidation.addUser.validate(req.body);
    if (error) return res.status(400).json({message: 'users.validationFail'});

    //Check if user exists
    const userExists = await User.findOne({
        where: {
            [Op.or]: [{username: req.body.username}, {email: req.body.email}]
        }
    });
    if(userExists) return res.status(400).json({code: 10, message: 'Email or user already exists!'});
    
    //Get rank
    const rank = await Rank.findOne({
        where: { id: req.body.rank }
    });
    if(!rank) return res.status(500).json({code: 12, message: 'Default rank not found. Please contact website administrator.'});

    //Hash passwords
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt);

    //Create user
    const user = await User.create({
        username: req.body.username,
        email: req.body.email,
        password: hashedPass,
        rank: rank.dataValues.id
    });
    if (!user) return res.status(500).json({message: '[Critical Error] Failed to add new user.'});

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'user_added', 'Dodano nowego uzytkownika ['+user.id+'] '+user.username+' z ranga o ID '+user.rank);

    res.status(200).json({message: 'user.creationSuccess'});
}

UsersController.updateUser = async (req, res) => {
    //Validate
    const params = userValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: 'form.error.validationFail'});

    const { error } = userValidation.updateUser.validate(req.body);
    if (error) return res.status(400).json({error});

    //Check if user exists
    const userExists = await User.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!userExists) return res.status(400).json({code: 10, message: 'There is no user in database with given ID!'});

    //Make query
    const query = {
        username: req.body.username,
        email: req.body.email,
        rank: req.body.rank
    };
    if(req.body.password) {
        const salt = await bcrypt.genSalt(10);
        const hashedPass = await bcrypt.hash(req.body.password, salt);
        query.password = hashedPass;
    }

    //Update user
    const user = await User.update(query,  {
        where: {
            id: req.params.id
        }
    });

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    let userlog_bonus = '';
    if(req.body.password) {
        userlog_bonus = ' (haslo zmienione)';
    }
    //UserLog.addLog(dToken.id, 'user_updated', 'Zaktualizowano uzytkownika ['+userExists.id+'] ('+userExists.username+', '+userExists.email+', '+userExists.rank+') do ('+req.body.username+', '+req.body.email+', '+req.body.rank+') '+userlog_bonus)
  
    return res.status(200).json({message: 'users.updateSuccess'});
    
}

UsersController.deleteUser = async (req, res) => {
    //Validate
    const params = userValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    //Delete user
    const user = await User.destroy({
        where: {
            id: req.params.id
        }
    });
    if(!user) return res.status(400).json({message: `Driver with ID ${req.params.id} not found.`});

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'user_deleted', 'Usunieto uzytkownika ['+user.id+']');

    res.status(200).json({message: 'Uzytkownik zostal usuniety.'});
}

module.exports = UsersController;