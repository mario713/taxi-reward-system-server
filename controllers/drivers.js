const Driver = require('../models/Driver');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const driverValidation = require('../validation/driver');
//const UserLog = require('../classes/logs/User');
const jwt = require('jsonwebtoken');

const DriversController = {}

DriversController.getAll = async (req, res) => {
    //Get all drivers
    const drivers = await Driver.findAll();
    res.status(200).json(drivers);
}

DriversController.getDriver = async (req, res) => {
    //Validate
    const { error } = driverValidation.getDriver.validate({id: req.params.id});
    if (error) return res.status(400).json({error});

    //Get driver with given ID
    const driver = await Driver.findOne({
        where: {
            id: req.params.id
        }
    });

    res.status(200).json(driver);
}

DriversController.addDriver = async (req, res) => {
    //Validate
    const { error } = driverValidation.addDriver.validate(req.body);
    if (error) return res.status(400).json({message: error});

    //Make query
    const query = {
        name: req.body.name,
        number: req.body.number
    };
    if (req.body.lastName) query.lastName = req.body.lastName;
    if (req.body.phoneNumber) query.phoneNumber = req.body.phoneNumber;

    //Create driver
    const driver = await Driver.create(query);    
    if (!driver) return res.status(500).json({message: '[Critical Error] Cant add driver, please contact administrator.'})
    
    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'driver_added', 'Dodano nowego kierowce ['+driver.id+'] '+driver.name+' '+driver.lastName+' o numerze '+driver.number);
    res.status(200).json({message: 'Driver has been added.'});
}

DriversController.updateDriver = async (req, res) => {
    //Validate
    const params = driverValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    const { error } = driverValidation.updateDriver.validate(req.body);
    if (error) return res.status(400).json({message: error});

    //Check if driver exists
    const driverExists = await Driver.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!driverExists) return res.status(400).json({code: 10, message: 'There is no driver in database with given ID!'});

    //Update driver
    const driver = await Driver.update({
        name: req.body.name,
        lastName: req.body.lastName,
        number: req.body.number,
        phoneNumber: req.body.phoneNumber
    },  {
        where: {
            id: req.params.id
        }
    });

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    UserLog.addLog(dToken.id, 'driver_updated', 'Zaktualizowano kierowce ['+driverExists.id+'] ('+driverExists.name+' '+driverExists.lastName+', '+driverExists.number+', '+driverExists.phoneNumber+') do ('+req.body.name+' '+req.body.lastName+', '+req.body.number+', '+req.body.phoneNumber+')');
  
    return res.status(200).json({message: `Driver ${req.params.id} updated.`});
    // if(driver != 0) return res.status(200).json({message: `Driver ${req.params.id} updated.`});
}

DriversController.deleteDriver = async (req, res) => {
    //Validate
    const params = driverValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    //Delete driver
    const driver = await Driver.destroy({
        where: {
            id: req.params.id
        }
    });
    if(!driver) return res.status(400).json({message: `Driver with ID ${req.params.id} not found.`});


    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //UserLog.addLog(dToken.id, 'driver_deleted', 'Usunieto kierowce ['+req.params.id+']')

    res.status(200).json({message: 'Driver has been deleted.'});
}

module.exports = DriversController;