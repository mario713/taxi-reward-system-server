const Course = require('../models/Course');
const Driver = require('../models/Driver');
const Client = require('../models/Client');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const courseValidation = require('../validation/course');
const jwt = require('jsonwebtoken');
//const ClientLog = require('../classes/logs/Client');
//const UserLog = require('../classes/logs/User');
//const DriverLog = require('../classes/logs/Driver');

const CoursesController = {}

CoursesController.getLast = async (req, res) => {
    const courses = await Course.findAll({
        limit: 10,
        order: [
            ['id', 'DESC']
        ]
    });

    if(!courses) { return res.status(400).json({message: 'Database error.'}); }

    return res.status(200).json(courses);
}

CoursesController.getCourses = async (req, res) => {
    //Validate
    const { error } = courseValidation.getCourses.validate({page: req.params.page, limit: req.params.limit});
    if (error) return res.status(400).json({message: error});

    const courses = await Course.findAll({
        order: [
            ['id', 'DESC']
        ],
        offset: Number(req.params.page) * Number(req.params.limit),
        limit: Number(req.params.limit)
    });
    if(!courses) return res.status(400).json({message: 'Database error.'});

    return res.status(200).json(courses);
}

CoursesController.addCourse = async (req, res) => {
    //Validate
    const { error } = courseValidation.addCourse.validate(req.body);
    if (error) return res.status(400).json({error});

    //Check if driver exists
    const driver = await Driver.findOne({
        where: {
            id: req.body.driver
        }
    })
    if(!driver) return res.status(400).json({message: 'Nie znaleziono kierowcy.'});

    //Check if client exists and create if doesnt
    let client;
    client = await Client.findOne({
        where: {
            number: req.body.client
        }
    });
    if(!client) {
    client = await Client.create({
            number: req.body.client,
            gratis: 0,
            lastModified: Date.now()
        });
        if(!client) return res.status(400).json({message: 'Database error.'});
    }

    //Create course
    const course = await Course.create({
        driver: req.body.driver,
        client: req.body.client,
        date: Date.now()
    });
    if(!course) return res.status(400).json({message: 'Database error.'});

    //Update client gratis
    const updatedClient = await Client.update({
        gratis: (client.dataValues.gratis + 1),
        lastModified: Date.now()
    },  {
        where: {
            id: client.dataValues.id
        }
    });

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //ClientLog.addLog(client.id, client.number, 'course_added', '['+dToken.id+'] '+dToken.username+' dodal nowy ['+course.id+'] kurs, kierowca: ['+driver.id+'] '+driver.name+' '+driver.lastName);
    //DriverLog.addLog(driver.id, 'course_added', '['+dToken.id+'] '+dToken.username+' dodal nowy ['+course.id+'] kurs, numerek klienta: '+client.number)
    //UserLog.addLog(dToken.id, 'course_added', 'Dodano ['+course.id+'] kurs dla numerka '+client.number+', kierowca: ['+driver.id+'] '+driver.name+' '+driver.lastName);

    return res.status(200).json({message: 'Kurs zostal dodany.'});
}

CoursesController.updateCourse = async (req, res) => {
    //Validate
    const params = courseValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    const { error } = courseValidation.editCourse.validate(req.body);
    if (error) return res.status(400).json({error});

    //Get current course
    const currentCourse = await Course.findOne({
        where: {
            id: req.params.id
        }
    });
    if(!currentCourse) return res.status(400).json({message: 'Database error.'});

    let driver, currentClient, client;

    if(currentCourse.dataValues.driver != req.body.driver) {
        //Check if driver exists
        driver = await Driver.findOne({
            where: {
                id: req.body.driver
            }
        });
        if(!driver) return res.status(400).json({message: 'Nie znaleziono kierowcy.'});
    }
    
    
    if(currentCourse.dataValues.client != req.body.client) {
        //Check if client exists and create if doesnt
        client = await Client.findOne({
            where: {
                number: req.body.client
            }
        });
        if(!client) {
            const client = await Client.create({
                number: req.body.client,
                gratis: 0
            });
            if(!client) return res.status(400).json({message: 'Database error.'});
        }

        currentClient = await Client.findOne({
            where: {
                number: currentCourse.dataValues.client
            }
        });
        if(!currentClient) return res.status(400).json({message: 'Database error.'});
    }

    //Update course
    const course = await Course.update({
        driver: req.body.driver,
        client: req.body.client
        //date: Date.now()
    }, {
        where: {
            id: req.params.id
        }
    });
    if(!course) return res.status(400).json({message: 'Database error.'});

    if(currentCourse.dataValues.client != req.body.client) {
        //Update current course client gratis
        let ccGratis = currentClient.dataValues.gratis - 1;
        if(ccGratis < 0) ccGratis = 0;
        const ccUpdated = await Client.update({
            gratis: ccGratis,
            lastModified: Date.now()
        },  {
            where: {
                number: currentCourse.dataValues.client
            }
        });

        //Update client gratis
        const updatedClient = await Client.update({
            gratis: (client.dataValues.gratis + 1),
            lastModified: Date.now()
        },  {
            where: {
                id: client.dataValues.id
            }
        });
    }

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    if(currentCourse.number != req.body.number) {
        //ClientLog.addLog(currentCourse.client, client.number, 'course_changed_from', 'Kurs ['+currentCourse.id+'] zostal zaktualizowany dla innego klienta');
        //ClientLog.addLog(updatedClient.id, client.number, 'course_updated_to', 'Kurs ['+currentCourse.id+'] zostal zaktualizowany od innego klienta')
    }
    if(currentCourse.driver != req.body.driver) {
        //DriverLog.addLog(currentCourse.driver, 'course_changed_from', 'Kurs ['+currentCourse.id+'] zostal zaktualizowany dla innego kierowcy');
        //DriverLog.addLog(req.body.driver, 'course_changed_to', 'Kurs ['+currentCourse.id+'] zostal zaktualizowany od innego kierowcy');
    }
    //ClientLog.addLog(client.id, 'course_updated', '['+dToken.id+'] '+dToken.username+' zaktualizowal kurs');
    //UserLog.addLog(dToken.id, 'course_updated', 'Zaktualizowano kurs o ID ['+currentCourse.id+']');

    return res.status(200).json({message: `Kurs od ID ${req.params.id} zostal edytowany.`});
}

CoursesController.getCourse = async (req, res) => {
    //Validate
    const params = courseValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    const course = await Course.findOne({
        where: {
            id: req.params.id
        }
    });

    if(!course) return res.status(400).json({message: 'Database error.'});

    return res.status(200).json(course);
}

CoursesController.deleteCourse = async (req, res) => {
    //Validate
    const params = courseValidation.ID.validate({id: req.params.id});
    if(params.error) return res.status(400).json({message: params.error});

    //Delete
    const course = await Course.destroy({
        where: {
            id: req.params.id
        }
    });
    if(!course) return res.status(400).json({message: `Kurs o ID ${req.params.id} nie zostal znaleziony.`});

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //ClientLog.addLog(client.id, client.number, 'course_deleted', '['+dToken.id+'] '+dToken.username+' usunal kurs o ID ['+course.id+']');
    //UserLog.addLog(dToken.id, 'course_deleted', 'Usunieto kurs o ID ['+course.id+']');
    //DriverLog.addLog(req.body.driver, 'course_deleted', 'Usunieto kurs o ID ['+course.id+']')

    return res.status(200).json({message: `Kurs o ID ${req.params.id} zostal usuniety.`});
}

CoursesController.countPages = async (req, res) => {
    //Validate
    const { error } = courseValidation.countPages.validate({limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    //Count
    const courses = await Course.count();
    const pages = Math.ceil(courses / req.params.limit);

    return res.status(200).json(pages);
}

module.exports = CoursesController;