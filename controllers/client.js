const Client = require('../models/Client');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const clientValidation = require('../validation/client');
const jwt = require('jsonwebtoken');
//const UserLog = require('../classes/logs/User');
//const ClientLog = require('../classes/logs/Client');

const ClientController = {}

ClientController.getOne = async (req, res) => {
    //Validate
    const { error } = clientValidation.ID.validate({id: req.params.number});
    if (error) return res.status(400).json({message: error});

    const client = await Client.findOne({
        where: {
            number: req.params.number
        }
    });
    if(!client) return res.status(400).json({message: 'error.client.clientNotFound'});

    return res.status(200).json(client);
}

ClientController.getSome = async (req, res) => {

    //Validate
    const { error } = clientValidation.getPage.validate({page: req.params.page, limit: req.params.limit});
    if (error) return res.status(400).json({message: error});

    const clients = await Client.findAll({
        order: [
            ['lastModified', 'DESC']
        ],
        offset: Number(req.params.page) * Number(req.params.limit),
        limit: Number(req.params.limit)
    });
    if(!clients) return res.status(400).json({message: 'error.database'});

    return res.status(200).json(clients);
}

ClientController.getGratis = async (req, res) => {
    //Validate
    const { error } = clientValidation.getGratis.validate({page: req.params.page, limit: req.params.limit});
    if (error) return res.status(400).json({message: error});

    const clients = await Client.findAll({
        order: [
            ['lastModified', 'DESC']
        ],
        offset: Number(req.params.page) * Number(req.params.limit),
        limit: Number(req.params.limit),
        where: {
            gratis: {
                [Op.gte]: 10
            }
        }
    });
    if(!clients) return res.status(400).json({message: 'Database Error.'});

    return res.status(200).json(clients);
}

ClientController.countClientsPages = async (req, res) => {
    //Validate
    const { error } = clientValidation.countPages.validate({limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    const clients = await Client.count();
    const pages = Math.ceil(clients / req.params.limit);

    return res.status(200).json(pages);
}

ClientController.countGratisPages = async (req, res) => {
    //Validate
    const { error } = clientValidation.countPages.validate({limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    //Count
    const clients = await Client.count({
        where: {
            gratis: {
                [Op.gte]: 10
            }
        }
    });
    const pages = Math.ceil(clients / req.params.limit);

    return res.status(200).json(pages);
}

ClientController.useGratisCourse = async (req, res) => {
    //Validate
    const { error } = clientValidation.ID.validate({id: req.params.number});
    if(error) return res.status(400).json({message: error});

    client = await Client.findOne({
        where: {
            number: req.params.number
        }
    });
    if(!client) return res.status(400).json({message: 'Database error.'});

    if(client.dataValues.gratis < 10) return res.status(400).json({message: 'Klient nie posiada 10 kursow.'});

    const updateClient = await Client.update({
        gratis: 0
    }, {
        where: {
            number: req.params.number
        }
    });
    if(!updateClient) return res.status(400).json({message: 'Database error.'});


    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);

    //UserLog.addLog(dToken.id, 'gratis_used', 'Zatwierdzono wykorzystanie gratisowego kursu dla klienta ['+client.id+'] '+client.number);
    //ClientLog.addLog(client.id, client.number, 'gratis_used', 'Uzytkownik ['+dToken.id+'] '+dToken.username+' zatwierdzil wykorzystanie gratisowego kursu');
    return res.status(200).json({message: 'Kurs zostal wykorzystany.'});
}

ClientController.updateClient = async (req, res) => {
    //Validate
    const params = clientValidation.ID.validate({id: req.params.number});
    if(params.error) return res.status(400).json({message: params.error});

    const { error } = clientValidation.updateClient.validate(req.body);
    if (error) return res.status(400).json({message: error});

    //Check if client exists
    const clientExists = await Client.findOne({
        where: {
            number: req.params.number
        }
    });
    if(!clientExists) return res.status(400).json({message: 'Nie znaleziono klienta o podanym numerze.'});

    //Make query
    const query = { }
    if (req.body.gratis) query.gratis = req.body.gratis;
    if (req.body.name) query.name = req.body.name;
    if (req.body.lastName) query.lastName = req.body.lastName;
    if (req.body.phoneNumber) query.phoneNumber = req.body.phoneNumber;

    //Update Client
    const client = await Client.update(query,  {
        where: {
            number: req.params.number
        }
    });
    if(client === 0) return res.status(400).json({message: 'Database Error'});

    const token = req.header('auth-token');
    let dToken = jwt.decode(token, process.env.JWT_SECRET);
    //ClientLog.addLog(client.id, client.number, 'client_updated', 'Klient zaktualizowany przez uzytkownika ['+dToken.id+'] '+dToken.username);
    //UserLog.addLog(dToken.id, 'client_updated', 'Zaktualizowano klienta ['+client.id+'] '+req.params.number);

    return res.status(200).json({message: `Klient o numerze ${req.params.number} zostal zaktualizowany.`});
}

ClientController.findClients = async (req, res) => {
    clientsArray = [];

    //Validate
    const { error } = clientValidation.ID.validate({id: req.params.number});
    if(error) return res.status(200).json(clientsArray);

    const clients = await Client.findAll({
        attributes: ['number', 'gratis'],
        limit: 5,
        where: {
            number: {
                [Op.like]: `${req.params.number}%`
            }
        }
    });

    for(let index in Object.keys(clients)) {
        clientsArray.push(clients[index].number);
    }
    return res.status(200).json(clientsArray);
}

module.exports = ClientController;