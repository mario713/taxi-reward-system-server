const User = require('../models/User');
const Rank = require('../models/Rank');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const authValidation = require('../validation/auth');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UserSession = require('../classes/UserSession');
const Session = require('../models/Session');

const AuthController = {}

AuthController.register = async (req, res, next) => {

    //Validate
    const { error } = authValidation.register.validate(req.body);
    if (error) return res.status(400).json({error});

    //Check if user exists
    const userExists = await User.findOne({
        where: {
            [Op.or]: [{username: req.body.username}, {email: req.body.email}]
        }
    });
    if(userExists) return res.status(400).json({code: 10, message: 'Email or user already exists!'});
    
    //Get default rank
    const defaultRank = await Rank.findOne({
        where: { isDefault: 1 }
    });
    if(!defaultRank) return res.status(500).json({code: 12, message: 'Default rank not found. Please contant website administrator.'});

    //Hash passwords
    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt);

    //Create user
    const user = await User.create({
        username: req.body.username,
        email: req.body.email,
        password: hashedPass,
        rank: defaultRank.dataValues.id
    });
    if (!user) return res.status(500).json({message: '[Critical Error] Failed to add new user.'});

    res.status(200).json({message: 'User has been created.'});
}

AuthController.login = async (req, res, next) => {
    //Validate
    const { error } = authValidation.login.validate(req.body);
    console.log(req.body);
    if(error) return res.status(400).json({error: 13, message: `Validation Error: ${error.details[0].message}`});
    

    //Check if user exists and get data
    const userData = await User.findOne({
        where: {
            username: req.body.username
        }
    });
    if(!userData) return res.status(400).json({code: 11, message: 'User not found in database.'});

    //Check password
    const validPass =  await bcrypt.compare(req.body.password, userData.password);
    if(!validPass) return res.status(400).json({message: 'Username or password is incorrect.'});

    //Generate session and add it to databse
    let sessid = UserSession.generate();
    let ip = req.ip;
    let ipsplit = ip.split(':');
    ip = ipsplit[3];
    let userAgent = req.get('User-Agent');
    UserSession.save(sessid, userData.id, ip, userAgent);

    //Create and assign a token
    const token = jwt.sign({
        id: userData.id,
        username: userData.username,
        sessionid: sessid
    }, process.env.JWT_SECRET, {expiresIn: '365d'});
    res.status(200).json({token: token, expires: jwt.decode(token).exp});
}

AuthController.logout = async(req, res) => {
    const token = req.header('auth-token');
    const verified = jwt.verify(token, process.env.JWT_SECRET);
    let dToken = jwt.decode(token, process.env.JWT_SECRET);

    try {
        UserSession.delete(dToken.sessionid, dToken.id);
        return res.status(200).json({message: 'Success: Logged out.'});
    } catch(err) {
        return res.status(400).json({message: 'Error: Cant logout.'});
    }
    
}

module.exports = AuthController;