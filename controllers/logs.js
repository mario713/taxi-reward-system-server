const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const LogsValidation = require('../validation/logs');
const UserLog = require('../models/UserLog');
const ClientLog = require('../models/ClientLog');
const DriverLog = require('../models/DriverLog');

const LogsController = {}

LogsController.getUserLogs = async (req, res) => {
    //Validate
    const { error } = LogsValidation.getLogs.validate({userid: req.params.id, page: req.params.page, limit: req.params.limit});
    if (error) return res.status(400).json({message: error});

    const userLogs = await UserLog.findAll({
        where: {
            userid: req.params.id
        },
        order: [
            ['time', 'DESC']
        ],
        offset: Number(req.params.page) * Number(req.params.limit),
        limit: Number(req.params.limit)
    });
    if(!userLogs) return res.status(400).json({message: 'error.database'});

    return res.status(200).json(userLogs);
}

LogsController.getUserLogPages = async (req, res) => {
    //Validate
    const { error } = LogsValidation.countPages.validate({id: req.params.id, limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    //Count
    const userLogs = await UserLog.count({
        where: {
            userid: req.params.id
        }
    });
    const pages = Math.ceil(userLogs / req.params.limit);

    return res.status(200).json(pages);
}

LogsController.getDriverLogs = async (req, res) => {
        //Validate
        const { error } = LogsValidation.getLogs.validate({userid: req.params.id, page: req.params.page, limit: req.params.limit});
        if (error) return res.status(400).json({message: error});
    
        const driverLogs = await DriverLog.findAll({
            where: {
                driverid: req.params.id
            },
            order: [
                ['time', 'DESC']
            ],
            offset: Number(req.params.page) * Number(req.params.limit),
            limit: Number(req.params.limit)
        });
        if(!driverLogs) return res.status(400).json({message: 'error.database'});
    
        return res.status(200).json(driverLogs);
}

LogsController.getDriverLogPages = async (req, res) => {
    //Validate
    const { error } = LogsValidation.countPages.validate({id: req.params.id, limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    //Count
    const driverLogs = await DriverLog.count({
        where: {
            driverid: req.params.id
        }
    });
    const pages = Math.ceil(driverLogs / req.params.limit);

    return res.status(200).json(pages);
}

LogsController.getClientLogs = async (req, res) => {
        //Validate
        const { error } = LogsValidation.getLogs.validate({userid: req.params.number, page: req.params.page, limit: req.params.limit});
        if (error) return res.status(400).json({message: error});
    
        const clientLogs = await ClientLog.findAll({
            where: {
                number: req.params.number
            },
            order: [
                ['time', 'DESC']
            ],
            offset: Number(req.params.page) * Number(req.params.limit),
            limit: Number(req.params.limit)
        });
        if(!clientLogs) return res.status(400).json({message: 'error.database'});
    
        return res.status(200).json(clientLogs);
}

LogsController.getClientLogPages = async (req, res) => {
    //Validate
    const { error } = LogsValidation.countPages.validate({id: req.params.number, limit: req.params.limit});
    if(error) return res.status(400).json({message: error});

    //Count
    const clientLogs = await ClientLog.count({
        where: {
            number: req.params.number
        }
    });
    const pages = Math.ceil(clientLogs / req.params.limit);

    return res.status(200).json(pages);
}

module.exports = LogsController;