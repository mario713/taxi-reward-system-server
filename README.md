# Taxi Reward System API

#### Example .env file:

```
DB_HOST=database_host
DB_USER=database_user
DB_PASS=database_password
DB_NAME=database_name
TIMEZONE=Europe/London
JWT_SECRET=put_jwt_secret_key_here
```

#### Start Server Command

`npm run start`
