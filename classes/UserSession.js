const crypto = require('crypto');
const Session = require('../models/Session');
const UserSession = {}

UserSession.generate = () => {
    let sessionid = crypto.randomBytes(32).toString('hex');
    sessionid = crypto.createHash('md5').update(sessionid).digest('hex');
    
    return sessionid;
}

UserSession.save = async (sessid, userid, ip, useragent) => {

    const sess = await Session.create({
        sessionid: sessid,
        userid: userid,
        ip: ip,
        time: Date.now(),
        useragent: useragent
    });
    if(!sess) { console.log('error'); } else { console.log('success'); }

}

UserSession.validate = async (sessionid, userid) => {
    const sess = await Session.findOne({
        where: {
            sessionid: sessionid,
            userid: userid
        }
    })
    if(!sess || sess === null) { return false; } else { return true; }
}

UserSession.delete = async (sessionid, userid) => {
    const sess = await Session.destroy({
        where: {
            sessionid: sessionid,
            userid: userid
        }
    });
    if(!sess) { return false; } else { return true; }
}

module.exports = UserSession;