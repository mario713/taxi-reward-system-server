const Sequelize = require('sequelize');
const db = require('../config/database');

const Session = db.define('session', {
    sessionid: {
        type: Sequelize.STRING(32),
        primaryKey: true,
        unique: true
    },
    userid: {
        type: Sequelize.INTEGER(10)
    },
    ip: {
        type: Sequelize.STRING(40)
    },
    time: {
        type: Sequelize.BIGINT(30)
    },
    useragent: {
        type: Sequelize.STRING(100)
    }
});

module.exports = Session;