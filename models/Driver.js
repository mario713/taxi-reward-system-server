const Sequelize = require('sequelize');
const db = require('../config/database');

const Driver = db.define('driver', {
    name: {
        type: Sequelize.STRING
    },
    lastName: {
        type: Sequelize.STRING
    },
    number: {
        type: Sequelize.STRING
    },
    phoneNumber: {
        type: Sequelize.INTEGER, 
        default: null
    }
});

module.exports = Driver;