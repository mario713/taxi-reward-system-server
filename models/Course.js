const Sequelize = require('sequelize');
const db = require('../config/database');

const Course = db.define('course', {
    driver: {
        type: Sequelize.INTEGER
    },
    client: {
        type: Sequelize.INTEGER
    },
    date: {
        type: Sequelize.BIGINT
    }
});

module.exports = Course;