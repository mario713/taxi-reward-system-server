const Sequelize = require('sequelize');
const db = require('../config/database');

const Setting = db.define('setting', {
    title: {
        type: Sequelize.STRING
    },
    language: {
        type: Sequelize.STRING
    }
});

module.exports = Setting;