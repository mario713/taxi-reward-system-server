const Sequelize = require('sequelize');
const db = require('../config/database');

const ClientLogs = db.define('client_log', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true
    },
    clientid: {
        type: Sequelize.INTEGER(11)
    },
    number: {
        type: Sequelize.INTEGER(11),
        defaultValue: 0
    },
    time: {
        type: Sequelize.BIGINT(30)
    },
    type: {
        type: Sequelize.STRING(30)
    },
    log: {
        type: Sequelize.TEXT
    }
});

module.exports = ClientLogs;