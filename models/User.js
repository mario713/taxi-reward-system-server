const Sequelize = require('sequelize');
const db = require('../config/database');

const User = db.define('user', {
    username: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    rank: {
        type: Sequelize.INTEGER, 
    },
    passReminderToken: {
        type: Sequelize.STRING,
        defaultValue: null
    }
});

module.exports = User;