const Sequelize = require('sequelize');
const db = require('../config/database');

const Rank = db.define('rank', {
    name: {
        type: Sequelize.STRING
    },
    color: {
        type: Sequelize.STRING
    },
    perms: {
        type: Sequelize.TEXT,
    },
    isDefault: {
        type: Sequelize.INTEGER
    },
    isPrime: {
        type: Sequelize.TINYINT
    }
});

module.exports = Rank;