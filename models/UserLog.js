const Sequelize = require('sequelize');
const db = require('../config/database');

const UserLogs = db.define('user_log', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true
    },
    userid: {
        type: Sequelize.INTEGER(11)
    },
    time: {
        type: Sequelize.BIGINT(30)
    },
    type: {
        type: Sequelize.STRING(30)
    },
    log: {
        type: Sequelize.TEXT
    }
});

module.exports = UserLogs;