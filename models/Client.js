const Sequelize = require('sequelize');
const db = require('../config/database');

const Client = db.define('client', {
    number: {
        type: Sequelize.INTEGER
    },
    gratis: {
        type: Sequelize.INTEGER,
        default: 0
    },
    name: {
        type: Sequelize.STRING,
        default: null
    },
    lastName: {
        type: Sequelize.STRING,
        default: null
    },
    phoneNumber: {
        type: Sequelize.STRING,
        default: null
    },
    lastModified: {
        type: Sequelize.BIGINT,
        default: null
    }
});

module.exports = Client;